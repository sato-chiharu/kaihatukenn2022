<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>配列練習_02</title>
    </head>
    <body>
        <?php
            $dog = array ("ビーグル", "ゴールデン", "ダックスフンド", "柴犬", "ポメラニアン", "シュナウザー");
            var_dump($dog);
            for($i=0; $i < count($dog) ; $i++){
                echo $dog[$i] . "<br/>";
            }
        ?>
        <?php
            $dog = array ("ビーグル", "ゴールデン", "ダックスフンド", "柴犬", "ポメラニアン", "シュナウザー");
            var_dump($dog);
            echo "<hr>";
            foreach($dog as $each){
            echo $each . "<br/>";
            }
                echo "<hr>";
            foreach($dog as $key => $value){
                echo $key . "番目の要素は" . $value . "です。<br/>";
            }
        ?>
        <?php
            $dog = array ("ビーグル", "ゴールデン", "ダックスフンド", "柴犬", "ポメラニアン", "シュナウザー");
            var_dump($dog);
            $needle = "柴犬";
            if(in_array($needle, $dog)) {
                echo $needle . " がdogの要素の値に存在しています";
            } else {
                echo $needle . " がdogの要素の値に存在しません";
            }
        ?>
    </body>
</html>
