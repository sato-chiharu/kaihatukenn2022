<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>配列練習_01</title>
    </head>
    <body>
        <?php
            $dog = ["ビーグル", "ゴールデン", "ダックスフンド", "柴犬", "ポメラニアン", "シュナウザー"];
            echo $dog[3];
            echo $dog[0];
            echo $dog[5];
            echo $dog[1];
            echo $dog[9];
            $dog[2] = "コーギー";
            $dog[6] = "チャウチャウ";
            ?>
            <pre>
            <?php var_dump($dog);
             ?>
            </pre>
    </body>
</html>
