<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>ループページ_03</title>
    </head>
    <body>
        <form method='get' action='loop03.php'>
            <input class="inputs" type="text" name="01_namae">行×<input class="inputs" type="text" name="02_namae">列<br>

            <input type="submit" value="送信">
            <input type="reset" value="リセット" />
        </form>
        <table border="1" cellpadding="6" cellspacing="0">
            <?php
                for($i=1; $i <=$_GET['01_namae']; $i++){
                    echo "<tr>";
                    for($j=1; $j <=$_GET['02_namae']; $j++){
                        echo "<td>".$i."-".$j."</td>";
                    }
                    echo "</tr>";
                }
            ?>
        </table>
    </body>
</html>
