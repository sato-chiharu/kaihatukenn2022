<!DOCTYPE html>
<?php
  // common
  // include("./include/functions.php");
  $DB_DSN = "mysql:host=localhost; dbname=csato; charset=utf8";
  $DB_USER = "webaccess";
  $DB_PW = "toMeu4rH";
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
  // $pdo = initDB();

  // 実行する生SQL文
  $query_str = "SELECT *
                FROM test_table";

  // $query_str = "SELECT * FROM `test_table` WHERE dish_name LIKE '%の%' AND genre = 'おつまみ'";

  echo $query_str; // 実行したSQLを画面に表示
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();

 ?>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- <link rel="stylesheet" href="./include/style.css"> -->
            <title>居酒屋メニューDB課題02</title>
    </head>
    <body>
        <h1>居酒屋ウェブレッジ</h1>
            <table border="1" cellpadding="6" cellspacing="0">
                <tr>
                    <th>料理名</th>
                    <th>ジャンル</th>
                    <th>値段</th>
                    <th>メモ</th>
                </tr>
                <?php
                    foreach($result as $each){
                        echo  "<tr>";
                        echo "<td>".$each['ryouriname']."</td>";
                        echo "<td>".$each['genre']."</td>";
                        echo "<td>".$each['price']."円"."</td>";
                        echo "<td>".$each['memo']."</td>";
                        echo "</tr>";
                    }
               ?>
            </table>
    </body>
</html>
