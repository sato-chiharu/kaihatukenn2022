<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>２次元配列練習_01</title>
    </head>
    <body>
        <h1>星野源アルバム</h1>
            <table border="1" cellpadding="6" cellspacing="0">
                <?php
                    $album_01 = array("ばらばら", "老夫婦", "くせのうた", "穴を掘る", "ばかのうた");
                    $album_02 = array("エピソード", "湯気", "くだらないの中に", "ステップ", "日常");
                    $album_03 = array("化物", "夢の外へ", "フィルム", "パロディ", "知らない");
                    $album_04 = array("時よ", "Week End", "SUN", "地獄でなぜ悪い", "桜の森");
                    $album_05 = array("Pop Virus", "恋", "肌", "アイデア", "Hello Song");

                    $album_all = array($album_01,$album_02,$album_03,$album_04,$album_05);

                    foreach($album_all as $value){
                        echo "<tr>";
                        foreach($value as $key){
                            echo "<td>".$key."</td>";
                        }
                        echo "</tr>";
                    }
                ?>
        <pre>
            <?php var_dump($album_all);?>
        </pre>
    </body>
</html>
