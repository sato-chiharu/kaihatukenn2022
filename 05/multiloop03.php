<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>2次元配列練習_03</title>
    </head>
    <body>
        <h1>星野源アルバム</h1>
            <table border="1" cellpadding="6" cellspacing="0">
                <?php
                    $album_01 = array(
                        "id" => "1",
                        "title" => "ばかのうた",
                        "songs" => "15",
                        "year" => "2010",
                    );
                    $album_02 =  array(
                        "id" => "2",
                        "title" => "エピソード",
                        "songs" => "13",
                        "year" => "2011",
                    );
                    $album_03 =  array(
                        "id" => "3",
                        "title" => "stranger",
                        "songs" => "13",
                        "year" => "2013",
                    );
                    $album_04 =  array(
                        "id" => "4",
                        "title" => "YELLOW DANCER",
                        "songs" => "14",
                        "year" => "2015",
                    );
                    $album_05 =  array(
                        "id" => "5",
                        "title" => "POP VIRUS",
                        "songs" => "14",
                        "year" => "2018",
                    );
                    $album_all = array($album_01,$album_02,$album_03,$album_04,$album_05);
                        foreach($album_all as $each){
                            echo "</tr>";
                                echo
                                    "<td>".$each["id"]."</td>".
                                    "<td>".$each["title"]."</td>".
                                    "<td>".$each["songs"]."</td>".
                                    "<td>".$each["year"]."</td>";
                            echo "</tr>";
                       }
                ?>
    </body>
</html>
