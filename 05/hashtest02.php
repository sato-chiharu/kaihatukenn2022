<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>連想配列練習_02</title>
    </head>
    <body>
        <?php
            $me_data = array(
                'dog' => 'ビーグル',
                'hobby' => 'フィルムカメラ',
                'town' => '福島',
                'age' => '22',
                'food' => '餃子'
                );
                foreach($me_data as $each){
                    echo $each . "<br/>";
                }
                foreach($me_data as $key => $value){
                    echo $key . " : " . $value . "<br/>";
                }
        ?>
        <pre>
        <?php var_dump($me_data); ?>
        </pre>
    </body>
</html>
