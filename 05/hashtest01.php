<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>連想配列練習_01</title>
    </head>
    <body>
        <?php
            $me_data = array(
                'dog' => 'ビーグル',
                'hobby' => 'フィルムカメラ',
                'town' => '福島',
                'age' => '22',
                'food' => '餃子'
                );
                    echo $me_data['town'];
                    echo $me_data['age'];
                    echo $me_data['dog'];
                    $me_data['age'] = '23';
        ?>
        <pre>
        <?php var_dump($me_data); ?>
        </pre>
    </body>
</html>
